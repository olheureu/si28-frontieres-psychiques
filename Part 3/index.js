filterMusic = document.getElementById("filterMusic");
audioTrack= document.getElementById("audioTrack");
circle1 = document.getElementById("circle1");
leaves = document.getElementById("leaves");

function launchAudio(){
    filterMusic.style.display = "none";
    document.body.style.cursor = "none";
    leaves.style.cursor="none";
    audioTrack.play();
    launchStory();
}

function launchStory(){
    setTimeout(displayCircle, 4500, circle1);
    setTimeout(launchAppleGIF, 120000);
    setTimeout(launchFlowerGIF, 205000);
    setTimeout(goToCredits, 220000);
}

function displayCircle(circle){
    circle.style.display="inline";
}

function hideCircle(circle){
   circle.style.display="none";
}

function launchAppleGIF(){
    circle1.style.backgroundImage= "url('images/appleGIF.gif')";
}

function launchFlowerGIF(){
    circle1.style.backgroundImage= "url('images/flowerGIF.gif')";
}

function goToCredits(){
    window.location.href="../End/index.html";
}