//Jauge contrôle 
progressBar = document.getElementById("progress-fill");

window.addEventListener('load', updateControlBar(progressBar, 85));

function updateControlBar(progressBar, value){
    setTimeout(update_2, 500, progressBar, value);
}

function update_2(bar, val){
    bar.style.width = val + "%";
}


//Audio
audioCuisine = new Audio("../audios/Part1D_cuisine.mp3");
audioChoice1 = new Audio("../audios/Part1D_choix1.mp3");
audioChoice2 = new Audio("../audios/Part1D_choix2.mp3");
audioPiscine = new Audio("../audios/Part1D_piscine.mp3");
circle1 = document.getElementById("circle1");
circle2 = document.getElementById("circle2");
circle3 = document.getElementById("circle3");
rightChoice1 = document.getElementById("rightChoice1");
leftChoice1 = document.getElementById("leftChoice1");
rightChoice2 = document.getElementById("rightChoice2");
leftChoice2 = document.getElementById("leftChoice2");
waveWrapper = document.getElementById("waveWrapper");
waveMiddle = document.getElementById("waveMiddle");
waveBottom = document.getElementById("waveBottom");
waveTop = document.getElementById("waveTop");

var firstClick = 1;

window.addEventListener("click", launchStoryCuisine);
window.onload = hideCircle(circle1);


window.onload = changeCursor;
function changeCursor(){
    document.body.style.cursor="pointer";
}

function launchStoryCuisine(){

    if(firstClick){
        document.body.style.cursor = "none";
    
        audioCuisine.volume = 0.7;
        audioCuisine.play();

        circle1.style.background = "#c235de"; 
        circle3.style.background = "white"; 

        circle1.style.left="20%";
        circle3.style.left="60%";

        setTimeout(displayCircle, 7000, circle1);
        setTimeout(hideCircle, 11000, circle1);

        setTimeout(displayCircle, 11000, circle3);
        setTimeout(hideCircle, 16000, circle3);

        setTimeout(displayCircle, 16000, circle1);
        setTimeout(hideCircle, 19000, circle1);

        setTimeout(launchChoiceCuisine, 20000);
    }

    firstClick = 0;
}

function displayCircle(circle){
     circle.style.display="inline";
 }

function hideCircle(circle){
    circle.style.display="none";
}

function launchChoiceCuisine(){
    rightChoice1.style.opacity = "1";
    leftChoice1.style.opacity = "1";
    document.body.style.cursor = "auto";
    rightChoice1.style.cursor = "pointer";
    leftChoice1.style.cursor = "pointer";
    rightChoice1.addEventListener("click", launchStoryPiscine);
    leftChoice1.addEventListener("click", audioChoiceCuisine);
}

function audioChoiceCuisine(){
    console.log("CUISINE");
    audioChoice1.volume = 1;
    audioChoice1.play();
    leftChoice1.style.opacity = "0";
    leftChoice1.style.cursor="none";
}

function launchStoryPiscine(){

    audioCuisine.pause();
    rightChoice1.style.opacity = "0";
    leftChoice1.style.opacity = "0";

    document.body.style.cursor = "none";
    leftChoice1.style.cursor= "none";
    rightChoice1.style.cursor= "none";


    audioPiscine.play();

    circle1.style.background = "#8A2BE2"; 
    circle2.style.background = "#5F9EA0";
    circle3.style.background = "white"; 

    circle1.style.left="8%";
    circle2.style.left="75%";
    circle3.style.left="42%";

    circle2.style.top="30%";

    setTimeout(displayCircle, 5000, circle1);
    setTimeout(hideCircle, 8500, circle1);
    setTimeout(launchAnim, 10000);

    setTimeout(displayCircle, 26000, circle2);
    setTimeout(hideCircle, 35000, circle2);

    setTimeout(displayCircle, 35500, circle3);
    setTimeout(hideCircle, 38000, circle3);

    setTimeout(launchChoicePiscine, 40000);

}

function launchChoicePiscine(){
    rightChoice1.style.display="none";
    leftChoice1.style.display="none";
    rightChoice2.style.display="inline";
    leftChoice2.style.display="inline";
    rightChoice2.style.opacity = "0";
    leftChoice2.style.opacity = "0";
    setTimeout(launchChoicePiscine_2, 500);
}

function launchChoicePiscine_2(){
    rightChoice2.style.opacity = "1";
    leftChoice2.style.opacity = "1";
    document.body.style.cursor = "auto";
    rightChoice2.style.cursor = "pointer";
    leftChoice2.style.cursor = "pointer";
    rightChoice2.addEventListener("click", beforeGrowingAnim);
    leftChoice2.addEventListener("click", audioChoicePiscine);
}

function audioChoicePiscine(){
    console.log("PISCINE");
    audioChoice2.play();
    leftChoice2.style.opacity = "0";
}

function launchAnim(){
    waveWrapper.style.opacity = "1";
    waveWrapper.style.cursor= "none";
    document.body.style.cursor= "none";
}

function beforeGrowingAnim(){
    rightChoice2.style.opacity = "0";
    leftChoice2.style.opacity = "0";
    setTimeout(growingAnim, 5000);
}

function growingAnim(){
    progressBar.style.display="none";
    waveMiddle.style.backgroundSize= "50% 320px";
    waveMiddle.style.animation= "move_wave 0.8s linear infinite";
    waveBottom.style.backgroundSize= "50% 280px";
    waveBottom.style.animation= "move_wave 0.8s linear infinite";  
    waveTop.style.backgroundSize= "50% 350px";
    waveTop.style.animation= "move_wave 0.8s linear infinite";
    setTimeout(opacityAnim, 100);
}

function opacityAnim(){
    waveWrapper.style.opacity="0";
    setTimeout(goToDiary, 10000);
}

function goToDiary(){
    window.location.href="../Part1T/Part1DT.html";
}