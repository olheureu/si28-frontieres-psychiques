//Jauge contrôle
progressBar = document.getElementById("progress-fill");

window.addEventListener('load', updateControlBar(progressBar, 95));

function updateControlBar(progressBar, value){
    setTimeout(update_2, 500, progressBar, value);
}

function update_2(bar, val){
    bar.style.width = val + "%";
}

//Audio 

audioTrack = new Audio("../audios/Part1H.mp3");
circle1 = document.getElementById("circle1");
circle2 = document.getElementById("circle2");
circle3 = document.getElementById("circle3");
var firstClick = 1;

window.addEventListener("click", launchStory);

window.onload = hideCircle(circle1);

window.onload = changeCursor;
function changeCursor(){
    document.body.style.cursor="pointer";
}

function launchStory(){

    if(firstClick){
        document.body.style.cursor="none";
        audioTrack.volume = 1;
        audioTrack.play();

        circle1.style.background = "grey"; 
        circle2.style.background = "grey"; 
        circle3.style.background = "white"; 
        circle4.style.background = "grey"; 
        circle5.style.background = "grey"; 

        circle1.style.left="3%";
        circle2.style.left="22%";
        circle3.style.left="40%";
        circle4.style.left="60%";

        setTimeout(displayCircle, 11500, circle1);
        setTimeout(hideCircle, 17500, circle1);

        setTimeout(displayCircle, 18000, circle2);
        setTimeout(hideCircle, 19500, circle2);

        setTimeout(displayCircle, 19500, circle1);
        setTimeout(hideCircle, 27500, circle1);

        setTimeout(displayCircle, 27700, circle4);
        setTimeout(hideCircle, 31000, circle4);

        setTimeout(displayCircle, 30700, circle2);
        setTimeout(hideCircle, 33500, circle2);

        setTimeout(displayCircle, 33500, circle4);
        setTimeout(hideCircle, 41500, circle4);

        setTimeout(launchAnim, 40500);

        setTimeout(displayCircle, 42000, circle3);
        setTimeout(hideCircle, 43500, circle3);
        
        setTimeout(displayCircle, 43500, circle4);
        setTimeout(hideCircle, 45000, circle4);

        setTimeout(displayCircle, 45500, circle5);
        setTimeout(hideCircle, 48000, circle5);
        
        setTimeout(displayCircle, 51200, circle5);
        setTimeout(hideCircle, 55000, circle5);

        setTimeout(displayCircle, 54800, circle4);
        setTimeout(hideCircle, 61500, circle4);

        setTimeout(displayCircle, 62000, circle3);
        setTimeout(hideCircle, 63000, circle3);

        setTimeout(displayCircle, 64000, circle4);
        setTimeout(hideCircle, 64500, circle4);

        setTimeout(displayCircle, 64000, circle3);
        setTimeout(hideCircle, 66000, circle3);

        setTimeout(displayCircle, 65500, circle4);
        setTimeout(hideCircle, 67000, circle4);
        
        setTimeout(hideAnim, 67000);

        setTimeout(displayCircle, 67500, circle3);
        setTimeout(hideCircle, 69000, circle3);

        setTimeout(redScreen, 67500);
    }

    firstClick = 0;
}

function displayCircle(circle){
     circle.style.display="inline";
 }

function hideCircle(circle){
    circle.style.display="none";
}

//Animation
line1 = document.getElementById("line1");
line2 = document.getElementById("line2");
line3 = document.getElementById("line3");
line4 = document.getElementById("line4");
line5 = document.getElementById("line5");

var firstClick = 1;

function launchAnim(){
    document.body.style.cursor="auto";
    setTimeout(displayText, 0   , line2);
    setTimeout(displayText, 4000, line3);
    setTimeout(displayText, 6500, line5);
    setTimeout(displayText, 10000, line1);
    setTimeout(displayText, 15000, line4);
}

function hideAnim(){
    line1.style.display="none";
    line2.style.display="none";
    line3.style.display="none";
    line4.style.display="none";
    line5.style.display="none";
}

function redScreen(){
    // circle3.style.background="red";
    document.body.style.background="red";
    setTimeout(goToDiary, 1500);
}

function displayText(line){
    line.style.opacity="1";
}

function changeText(text)
{
    let hoverBox = document.getElementById('G41');
    hoverBox.innerHTML = '';
    hoverBox.innerHTML = text;
}

function goToDiary(){
    window.location.href="../Part1T/Part1HT.html";
}
