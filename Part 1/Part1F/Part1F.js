//Jauge contrôle
progressBar = document.getElementById("progress-fill");

window.addEventListener('load', updateControlBar(progressBar, 50));

function updateControlBar(progressBar, value){
    setTimeout(update_2, 500, progressBar, value);
}

function update_2(bar, val){
    bar.style.width = val + "%";
}

//Audio

audioTrack = new Audio("../audios/Part1F_bis.mp3");
audioFocus = new Audio("../audios/Part1F_focus.mp3");

circle1 = document.getElementById("circle1");
circle2 = document.getElementById("circle2");
circle3 = document.getElementById("circle3");

var firstClick = 1;

window.addEventListener("click", launchStory);
window.onload = hideCircle(circle1);

window.onload = changeCursor;
function changeCursor(){
    document.body.style.cursor="pointer";
}

function launchStory(){

    if(firstClick){
        firstClick = 0;

        console.log("story launched");

        document.body.style.cursor = "none";
        audioTrack.volume = 0.5;
        audioTrack.play();

        focusButton.style.cursor="none";

        circle1.style.left="8%";
        circle3.style.left="75%";
        circle2.style.left="42%";

        setTimeout(displayCircle, 1000, circle3);
        setTimeout(hideCircle, 5000, circle3);

        setTimeout(displayCircle, 4500, circle2);
        setTimeout(hideCircle, 6400, circle2);

        setTimeout(displayCircle, 6800, circle3);
        setTimeout(hideCircle, 13500, circle3);

        setTimeout(launchAnim, 3000);

        setTimeout(displayCircle, 10500, circle2);
        setTimeout(hideCircle, 11500, circle2);

        setTimeout(displayCircle, 12500, circle2);
        setTimeout(hideCircle, 13500, circle2);

        setTimeout(displayCircle, 15500, circle1);
        setTimeout(hideCircle, 23300, circle1);

        setTimeout(displayCircle, 23500, circle3);
        setTimeout(hideCircle, 26200, circle3);

        setTimeout(displayCircle, 26200, circle1);
        setTimeout(hideCircle, 27700, circle1); 

        setTimeout(displayCircle, 28000, circle3);
        setTimeout(hideCircle, 29500, circle3);       
       
        setTimeout(displayCircle, 31500, circle2);
        setTimeout(hideCircle, 39000, circle2);  
        
        setTimeout(displayCircle, 30500, circle1);
        setTimeout(hideCircle, 31000, circle1);       
        
        setTimeout(displayCircle, 33500, circle1);
        setTimeout(hideCircle, 35500, circle1);    
        
        setTimeout(displayCircle, 39000, circle1);
        setTimeout(hideCircle, 41000, circle1); 

        setTimeout(displayCircle, 40000, circle3);
        setTimeout(hideCircle, 42000, circle3); 

        setTimeout(clearScreen, 43000);
    }

}

function displayCircle(circle){
     circle.style.display="inline";
 }

function hideCircle(circle){
    circle.style.display="none";
}


function goToDiary(){
    window.location.href="../Part1T/Part1FT.html";
}


//Animation

focusButton = document.getElementById("focusButton");
 myImg1 = document.getElementById("saucisson");
 myImg2 = document.getElementById("saucisse");
 myImg3 = document.getElementById("oeuf");
 myImg4 = document.getElementById("lait");
 myImg5 = document.getElementById("fromage");
 myImg6 = document.getElementById("carotte");
 myImg7 = document.getElementById("poulet");
 myImg8 = document.getElementById("poisson");
 myImg9 = document.getElementById("pain");
 myImg10 = document.getElementById("steak");
 myImg11 = document.getElementById("banane");
 myImg12 = document.getElementById("tomate");
 vignetsContainer= document.getElementById("vignetsContainer");

 firstButtonClick = 1;
 numberButtonClick = 0;

focusButton.addEventListener("click", growImages);

function launchAnim(){
    console.log("anim launched");

    setTimeout(lowOpacity, 0, myImg7);
    setTimeout(lowOpacity, 6000, myImg9);
    setTimeout(lowOpacity, 7500, myImg3);
    setTimeout(lowOpacity, 12500, myImg6);
    setTimeout(lowOpacity, 13000,myImg10);
    setTimeout(showButton, 16000);
    setTimeout(lowOpacity,15000, myImg11);
    setTimeout(lowOpacity, 18000, myImg4);
    setTimeout(lowOpacity, 20000, myImg1);
    setTimeout(lowOpacity, 23000, myImg12);
    setTimeout(lowOpacity, 26000, myImg8);
    setTimeout(lowOpacity, 29000, myImg5);
    setTimeout(lowOpacity, 32000 ,myImg2);
}

function lowOpacity(image){
    image.style.opacity="0.3";
}

function showButton(){
    document.body.style.cursor="auto";
    focusButton.style.cursor="pointer";
    focusButton.style.opacity = "1";
}

function growImages() {

    width1=myImg1.clientWidth;
    if (width1 >= 200) {
        audioFocus.play();

        myImg1.style.width = "500px";
        myImg2.style.width = "500px";
        myImg3.style.width = "500px";
        myImg4.style.width = "500px";
        myImg5.style.width = "500px";
        myImg6.style.width = "500px";
        myImg7.style.width = "500px";
        myImg8.style.width = "500px";
        myImg9.style.width = "500px";
        myImg10.style.width = "500px";
        myImg11.style.width = "500px";
        myImg12.style.width = "500px";

        myImg1.style.opacity = "1";
        myImg2.style.opacity = "1";
        myImg3.style.opacity = "1";
        myImg4.style.opacity = "1";
        myImg5.style.opacity = "1";
        myImg6.style.opacity = "1";
        myImg7.style.opacity = "1";
        myImg8.style.opacity = "1";
        myImg9.style.opacity = "1";
        myImg10.style.opacity = "1";
        myImg11.style.opacity = "1";
        myImg12.style.opacity = "1";
        
        focusButton.style.opacity = "0";

    } else {
        myImg1.style.width = "".concat(width1+20,"px");
        myImg2.style.width = "".concat(width1+20,"px");
        myImg3.style.width = "".concat(width1+20,"px");
        myImg4.style.width = "".concat(width1+20,"px");
        myImg5.style.width = "".concat(width1+20,"px");
        myImg6.style.width = "".concat(width1+20,"px");
        myImg7.style.width = "".concat(width1+20,"px");
        myImg8.style.width = "".concat(width1+20,"px");
        myImg9.style.width = "".concat(width1+20,"px");
        myImg10.style.width = "".concat(width1+20,"px");
        myImg11.style.width = "".concat(width1+20,"px");
        myImg12.style.width = "".concat(width1+20,"px");
    }
}

function clearScreen(){
    myImg1.style.opacity = "0";
    myImg2.style.opacity = "0";
    myImg3.style.opacity = "0";
    myImg4.style.opacity = "0";
    myImg5.style.opacity = "0";
    myImg6.style.opacity = "0";
    myImg7.style.opacity = "0";
    myImg8.style.opacity = "0";
    myImg9.style.opacity = "0";
    myImg10.style.opacity = "0";
    myImg11.style.opacity = "0";
    myImg12.style.opacity = "0";
    setTimeout(goToDiary, 5000);
}

function goToDiary(){
    window.location.href="../Part1T/Part1FT.html";
}