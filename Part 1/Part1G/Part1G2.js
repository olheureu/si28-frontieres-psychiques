//Jauge contrôle
progressBar = document.getElementById("progress-fill");

window.addEventListener('load', updateControlBar(progressBar, 70));

function updateControlBar(progressBar, value){
    setTimeout(update_2, 500, progressBar, value);
}

function update_2(bar, val){
    bar.style.width = val + "%";
}

//Animation
G40 = document.getElementById("G40");
G90 = document.getElementById("G90");
G44 = document.getElementById("G44");
G520 = document.getElementById("G520");
G300= document.getElementById("G300");
G400 = document.getElementById("G400");
G4500 = document.getElementById("G4500");
balance = document.getElementById("balance");
labelBalance = document.getElementById("labelBalance");
nbCal= document.getElementById("nbCal");
reset = document.getElementById("reset");
nextButton = document.getElementById("nextButton");

var firstClick = 1;

window.onload = launchAnim();

function launchAnim(){
        setTimeout(displayNumber, 300, G40);

        setTimeout(displayNumber, 1000, G90);

        setTimeout(displayNumber, 2000, G44);

        setTimeout(displayNumber, 2500, G520);

        setTimeout(displayNumber, 2900, G300);

        setTimeout(displayNumber, 3500, G400);

        setTimeout(displayNumber, 3700, G4500);

        setTimeout(displayBalance, 0, balance);

}

function displayNumber(number){
     number.style.display="block";
 }

 function displayBalance(b){
  b.style.opacity="1";
}

 function hideNumber(number){
     number.style.display="none";
}

// Drag
var compteur = 0;

function onDrop(event) {
  compteur= parseFloat(nbCal.innerHTML);
    const id = event
    .dataTransfer
    .getData('text');
    const draggableElement = document.getElementById(id);
    draggableElement.style.fontSize= "small"; 
    if (draggableElement==G40){
                                draggableElement.innerHTML="Orange";
                                draggableElement.style.top= "20%";
                                draggableElement.style.left= "10%";
                                nbCal.innerHTML = compteur + 67;}
    if (draggableElement==G90){ 
                                draggableElement.innerHTML="Banane";
                                draggableElement.style.top= "30%";
                                draggableElement.style.left= "40%";
                                nbCal.innerHTML = compteur + 89;
                                }
    if (draggableElement==G520){
                                draggableElement.innerHTML="Steak";
                                draggableElement.style.top= "5%";
                                draggableElement.style.left= "50%";
                                nbCal.innerHTML = compteur + 279;
                              }
    if (draggableElement==G300){
                                  draggableElement.innerHTML="Hamburger";
                                 draggableElement.style.top= "30%";
                                 draggableElement.style.left= "5%";
                                 nbCal.innerHTML = compteur + 1106;
                               }
    if (draggableElement==G44){
                                draggableElement.innerHTML="Yaourt";
                                draggableElement.style.top= "40%";
                                draggableElement.style.left= "30%";
                                nbCal.innerHTML = compteur + 54;}

    if (draggableElement==G400){
                                draggableElement.innerHTML="Sandwich";
                                draggableElement.style.top= "40%";
                                draggableElement.style.left= "60%";
                                nbCal.innerHTML = compteur + 760;}

    if (draggableElement==G4500){
                                  draggableElement.innerHTML="Ile flottante";
                                  draggableElement.style.top= "20%";
                                  draggableElement.style.left= "70%";
                                  nbCal.innerHTML = compteur + 168;}
    compteur=parseFloat(nbCal.innerHTML);
    if(compteur>210){
      draggableElement.style.color= "red";
      endAnim(0);
    }
    else{
      draggableElement.style.color= "green";
      endAnim(1);
    }
    const dropzone = event.target;
    dropzone.appendChild(draggableElement);
    event
    .dataTransfer
  }

  
  function endAnim(bool){
    if(bool==0){
        displayNumber(reset);
        hideNumber(nextButton);
    }
    if(bool==1){
      displayNumber(nextButton);
    }
  }

  function onDragOver(event) {
    event.preventDefault();
  }
  
  function onDragStart(event) {
    event
      .dataTransfer
      .setData('text/plain', event.target.id);
  
    event
      .currentTarget
      .style ;
  }

  function Full(element){
    if (compteur>3){
      var x=document.getElementById("G40")
      x.style.opacity="0%"
      setTimeout(change(element));

    } }

    