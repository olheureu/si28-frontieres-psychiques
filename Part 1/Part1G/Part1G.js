//Jauge contrôle
progressBar = document.getElementById("progress-fill");

window.addEventListener('load', updateControlBar(progressBar, 70));

function updateControlBar(progressBar, value){
    setTimeout(update_2, 500, progressBar, value);
}

function update_2(bar, val){
    bar.style.width = val + "%";
}

audioTrack = new Audio("../audios/Part1G.mp3");

circle1 = document.getElementById("circle1");
circle2 = document.getElementById("circle2");
circle3 = document.getElementById("circle3");
circle4 = document.getElementById("circle4");
var firstClick = 1;
reset = document.getElementById("reset");
var compteur = parseFloat(nbCal.innerHTML);
var bool = 0;
nextButton = document.getElementById("nextButtonG");

window.addEventListener("click", launchStory);

window.onload = changeCursor;
function changeCursor(){
    document.body.style.cursor="pointer";
}

function launchStory(){

    if(firstClick){
      console.log("in");
        document.body.style.cursor = "grab";

        audioTrack.play();
        audioTrack.volume = 0.75;

        circle3.style.background = "white"; 
        circle4.style.background = "#d9b3ff";

        launchAnim();

        setTimeout(displayCircle, 4500, circle1);
        setTimeout(hideCircle, 6000, circle1);

        setTimeout(displayCircle, 6000, circle4);
        setTimeout(hideCircle, 7500, circle4);

        setTimeout(displayCircle, 7500, circle2);
        setTimeout(hideCircle, 9500, circle2);

        setTimeout(displayCircle, 10000, circle1);
        setTimeout(hideCircle, 14000, circle1);

        setTimeout(displayCircle, 14500, circle2);
        setTimeout(hideCircle, 16000, circle2);

        setTimeout(displayCircle, 15500, circle4);
        setTimeout(hideCircle, 16000, circle4);

        setTimeout(displayCircle, 17500, circle1);
        setTimeout(hideCircle,20000, circle1);

        setTimeout(displayCircle, 19000, circle2);
        setTimeout(hideCircle, 27000, circle2);

        setTimeout(displayCircle, 27500, circle4);
        setTimeout(hideCircle, 28500, circle4);

        setTimeout(displayCircle, 28500, circle1);
        setTimeout(hideCircle, 31000, circle1);

        setTimeout(displayCircle, 32000, circle4);
        setTimeout(hideCircle, 36000, circle4);

        setTimeout(displayCircle, 37000, circle3);
        setTimeout(hideCircle, 43000, circle3);

        setTimeout(displayCircle, 43000, circle4);
        setTimeout(hideCircle, 47000, circle4);

        setTimeout(endAnim, 48000);
    }

    firstClick = 0;
}

function displayCircle(circle){
     circle.style.display="inline";
 }

function hideCircle(circle){
    circle.style.display="none";
}

//Animation
G40 = document.getElementById("G40");
G90 = document.getElementById("G90");
G44 = document.getElementById("G44");
G520 = document.getElementById("G520");
G300= document.getElementById("G300");
G400 = document.getElementById("G400");
G4500 = document.getElementById("G4500");
balance = document.getElementById("balance");
labelBalance = document.getElementById("labelBalance");
nbCal= document.getElementById("nbCal");
balanceAndScreen = document.getElementById("balanceAndScreen");
indication = document.getElementById("indication");

var firstClick = 1;

function launchAnim(){
        setTimeout(displayNumber, 0, G40);

        setTimeout(displayNumber, 1000, G90);

        setTimeout(displayNumber, 2000, G44);

        setTimeout(displayNumber, 2500, G520);

        setTimeout(displayNumber, 2900, G300);

        setTimeout(displayNumber, 3500, G400);

        setTimeout(displayNumber, 3700, G4500);

        setTimeout(displayBalance, 0, balance);
        setTimeout(displayBalance, 0, balanceAndScreen);
        setTimeout(displayBalance, 0, labelBalance);
        setTimeout(displayBalance, 0, indication);


}



function displayNumber(number){
     number.style.display="block";
 }

 function displayBalance(b){
  b.style.opacity="1";
}

 function hideNumber(number){
     number.style.display="none";
}

// Drag

var drag1=document.getElementById("G40")
var drag2=document.getElementById("G90")
var drag3=document.getElementById("G44")
var drag4=document.getElementById("G520")
var ok = 1;


function onDrop(event) {
  compteur= parseFloat(nbCal.innerHTML);
  const id = event
    .dataTransfer
    .getData('text');
    const draggableElement = document.getElementById(id);
    draggableElement.style.fontSize= "small"; 
    if (draggableElement==G40){
                                draggableElement.innerHTML="Orange";
                                draggableElement.style.top= "20%";
                                draggableElement.style.left= "10%";
                                nbCal.innerHTML = compteur + 67;}
    if (draggableElement==G90){ 
                                draggableElement.innerHTML="Banane";
                                draggableElement.style.top= "30%";
                                draggableElement.style.left= "40%";
                                nbCal.innerHTML = compteur + 89;}
    if (draggableElement==G520){
                                draggableElement.innerHTML="Steak";
                                draggableElement.style.top= "5%";
                                draggableElement.style.left= "50%";
                                nbCal.innerHTML = compteur + 279;
                              }
    if (draggableElement==G300){
                                  draggableElement.innerHTML="Hamburger";
                                 draggableElement.style.top= "30%";
                                 draggableElement.style.left= "5%";
                                 nbCal.innerHTML = compteur + 1106;}
    if (draggableElement==G44){
                                draggableElement.innerHTML="Yaourt";
                                draggableElement.style.top= "40%";
                                draggableElement.style.left= "30%";
                                nbCal.innerHTML = compteur + 54;}

    if (draggableElement==G400){
                                draggableElement.innerHTML="Sandwich";
                                draggableElement.style.top= "40%";
                                draggableElement.style.left= "60%";
                                nbCal.innerHTML = compteur + 760;}

    if (draggableElement==G4500){
                                  draggableElement.innerHTML="Ile flottante";
                                  draggableElement.style.top= "20%";
                                  draggableElement.style.left= "70%";
                                  nbCal.innerHTML = compteur + 168;}
    compteur=parseFloat(nbCal.innerHTML);
    if(compteur>210){
      draggableElement.style.color= "red";
    }
    else{
      draggableElement.style.color= "green";
      bool=1;
    }
    const dropzone = event.target;
    dropzone.appendChild(draggableElement);
    event
    .dataTransfer
  }

  function endAnim(){
    displayNumber(reset);
    if(compteur<=210 || bool){
      displayNumber(nextButton);
    }
  }

  function onDragOver(event) {
    event.preventDefault();
  }
  
  function onDragStart(event) {
    event
      .dataTransfer
      .setData('text/plain', event.target.id);
  
    event
      .currentTarget
      .style ;
  }

  function Full(element){
    if (compteur>3){
      var x=document.getElementById("G40")
      x.style.opacity="0%"
      setTimeout(change(element));

    } }





