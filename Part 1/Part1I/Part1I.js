//Jauge contrôle
progressBar = document.getElementById("progress-fill");

window.addEventListener('load', updateControlBar(progressBar, 0));

function updateControlBar(progressBar, value){
    setTimeout(update_2, 500, progressBar, value);
}

function update_2(bar, val){
    bar.style.width = val + "%";
}

//Audio
audioTrack = new Audio("../audios/Part1I.mp3");
circle3 = document.getElementById("circle3");
var firstClick = 1;

window.addEventListener("click", launchStory);

window.addEventListener("click", launchStory);
window.onload = hideCircle(circle1);
// window.onload = changeCursor();
// function changeCursor(){
//     document.body.style.cursor="pointer";
// }

function launchStory(){

    if(firstClick){
        document.body.style.cursor = "none";

        audioTrack.play();

        circle3.style.background = "white"; 
        circle3.style.left="40%";

        setTimeout(displayCircle, 3000, circle3);
        setTimeout(hideCircle, 4300, circle3);

        setTimeout(displayCircle, 7500, circle3);
        setTimeout(hideCircle, 9000, circle3);

        setTimeout(displayCircle, 9500, circle3);
        setTimeout(hideCircle, 14000, circle3);

        setTimeout(goToDiary, 37000);

    }

    firstClick = 0;
}

function displayCircle(circle){
     circle.style.display="inline";
 }

function hideCircle(circle){
    circle.style.display="none";
}

function goToDiary(){
    window.location.href="../Part1T/Part1IT.html";
}