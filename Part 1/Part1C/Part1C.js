//Jauge contrôle
progressBar = document.getElementById("progressC-fill");

 window.addEventListener('load', updateControlBar(progressBar, 50));

 function updateControlBar(progressBar, value){
     setTimeout(update_2, 500, progressBar, value);
 }

 function update_2(bar, val){
     bar.style.width = val + "%";
 }
 

 //Audio
waterDrop = document.getElementById("waterDrop");
drop = document.getElementById("drop");
wave = document.getElementById("wave");

audioTrack = new Audio("../audios/Part1C.mp3");
circle1 = document.getElementById("circle1");
circle2 = document.getElementById("circle2");
circle3 = document.getElementById("circle3");
var firstClick = 1;

window.addEventListener("click", launchStory);

window.onload = hideCircle(circle1);

window.onload = changeCursor;
function changeCursor(){
    document.body.style.cursor="pointer";
}

function launchStory(){

    if(firstClick){
        document.body.style.cursor = "none";

        audioTrack.volume = 0.7;
        audioTrack.play();

        circle3.style.background = "white"; 

        circle1.style.left="8%";
        circle2.style.left="75%";
        circle3.style.left="42%";

        setTimeout(displayCircle, 1700, circle2);
        setTimeout(hideCircle, 5000, circle2);

        setTimeout(displayCircle, 5500, circle3);
        setTimeout(hideCircle, 10000, circle3);

        setTimeout(displayCircle, 10010, circle1);
        setTimeout(hideCircle, 15000, circle1);

        setTimeout(displayCircle, 15300, circle2);
        setTimeout(hideCircle, 17000, circle2);

        setTimeout(displayCircle, 17800, circle3);
        setTimeout(hideCircle, 22100, circle3);

        setTimeout(displayCircle, 22100, circle1);
        setTimeout(hideCircle, 25500, circle1);

        setTimeout(displayCircle, 30500, circle3);
        setTimeout(changeColor, 33200, circle3);
        setTimeout(hideCircle, 34700, circle3);

        setTimeout(launchAnim, 35800);
    }

    firstClick = 0;
}

function displayCircle(circle){
     circle.style.display="inline";
 }

function hideCircle(circle){
    circle.style.display="none";
}

function changeColor(circle){
    circle.style.transition = "background 2s ease-in";
    circle.style.background = "#4a4a4d";
}

function launchAnim(){
    waterDrop.style.display = "block";
    setTimeout(stopAnim_1, 1900);
}

function stopAnim_1(){
    drop.style.display = "none";
    setTimeout(stopAnim_2, 8000);
}

function stopAnim_2(){
    wave.style.display = "none";
    setTimeout(goToDiary, 3500);
}

function goToDiary(){
    window.location.href="../Part1T/Part1CT.html";
}