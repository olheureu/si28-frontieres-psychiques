circle1 = document.getElementById("circle1");
circle2 = document.getElementById("circle2");
circle3 = document.getElementById("circle3");
circle4 = document.getElementById("circle4");

apple = document.getElementById("apple");

var firstClick = 1;

audioTrack = document.getElementById("audio");

window.addEventListener("click", launchStory);
window.onload = hideCircle(circle1);
window.onload = changeCursor;
function changeCursor(){
    document.body.style.cursor="pointer";
}

apple.addEventListener("click", appleDisparition);

function launchStory(){

    if(firstClick){
        document.body.style.cursor = "none";
        audioTrack.volume = 0.75;
        audioTrack.play();

        setTimeout(displayCircle, 3500, circle1);
        setTimeout(hideCircle, 4500, circle1);

        setTimeout(displayCircle, 4700, circle2);
        setTimeout(hideCircle, 16000, circle2);

        setTimeout(displayCircle, 16200, circle3);
        setTimeout(hideCircle, 20500, circle3);

        setTimeout(displayCircle, 20500, circle1);
        setTimeout(hideCircle, 28000, circle1);

        setTimeout(displayCircle, 28500, circle2);
        setTimeout(hideCircle, 31000, circle2);

        setTimeout(displayCircle, 30000, circle1);
        setTimeout(hideCircle, 34500, circle1);

        setTimeout(displayCircle, 35500, circle3);
        setTimeout(hideCircle, 39500, circle3);

        setTimeout(displayCircle, 44000, circle4);
        setTimeout(hideCircle, 52500, circle4);
        setTimeout(showApple, 51500);
        
    }

    firstClick = 0;
}


function displayCircle(circle){
     circle.style.display="inline";
 }

function hideCircle(circle){
    circle.style.display="none";
}

function showApple(){
    apple.style.transform = "translate(-85vh)";
    document.body.style.cursor = "default";
}

function appleDisparition(){
    apple.style.opacity = "0";
    setTimeout(goToDiary, 2500);
}

function goToDiary(){
    window.location.href="../Part1T/Part1AT.html";
}
