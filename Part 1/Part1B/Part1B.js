//Jauge contrôle 
progressBar = document.getElementById("progress-fill");

window.addEventListener('load', updateControlBar(progressBar, 65));

function updateControlBar(progressBar, value){
    setTimeout(update_2, 500, progressBar, value);
}

function update_2(bar, val){
    bar.style.width = val + "%";
}

//Audio
audioTrack = new Audio("../audios/Part1B.mp3");
circle1 = document.getElementById("circle1");
circle2 = document.getElementById("circle2");
circle3 = document.getElementById("circle3");
circle4 = document.getElementById("circle4");
circle5 = document.getElementById("circle5");
var firstClick = 1;

window.addEventListener("click", launchStory);

window.addEventListener("click", launchStory);
window.onload = hideCircle(circle1);
window.onload = hidePains()

// window.onload = changeCursor;
// function changeCursor(){
//     document.body.style.cursor="grab";
// }

function launchStory(){

    if(firstClick){
        document.body.style.cursor = "none";

        audioTrack.play();

        circle1.style.background = "#82f008";
        circle2.style.background = "blue";
        circle3.style.background = "white"; 
        circle4.style.background = "yellow";
        circle5.style.background = "#c235de";

        circle1.style.left="3%";
        circle2.style.left="22%";
        circle3.style.left="40%";
        circle4.style.left="60%";

        setTimeout(displayCircle, 3500, circle1);
        setTimeout(hideCircle, 5500, circle1);

        setTimeout(displayCircle, 5500, circle2);
        setTimeout(hideCircle, 7000, circle2);

        setTimeout(displayCircle, 7000, circle4);
        setTimeout(hideCircle, 9100, circle4);

        setTimeout(displayCircle, 9100, circle1);
        setTimeout(hideCircle, 12300, circle1);

        setTimeout(displayCircle, 12400, circle2);
        setTimeout(hideCircle, 17500, circle2);

        setTimeout(displayCircle, 18000, circle3);
        setTimeout(hideCircle, 20000, circle3);

        setTimeout(displayCircle, 19600, circle5);
        setTimeout(hideCircle, 22500, circle5);

        setTimeout(displayCircle, 22500, circle1);
        setTimeout(hideCircle, 23500, circle1);
        setTimeout(displayCircle, 22500, circle2);
        setTimeout(hideCircle, 23500, circle2);
        setTimeout(displayCircle, 22500, circle4);
        setTimeout(hideCircle, 23500, circle4);

        setTimeout(displayCircle, 23500, circle4);
        setTimeout(hideCircle, 24400, circle4);
        
        setTimeout(displayCircle, 24500, circle2);
        setTimeout(hideCircle, 25500, circle2);

        setTimeout(displayCircle, 26000, circle4);
        setTimeout(hideCircle, 26500, circle4);

        setTimeout(preLaunchAnim, 26500);
    }

    firstClick = 0;
}

function displayCircle(circle){
     circle.style.display="inline";
 }

function hideCircle(circle){
    circle.style.display="none";
}



//Animation
pain1 = document.getElementById("pain1");
pain2 = document.getElementById("pain2");
pain3 = document.getElementById("pain3");
pain4 = document.getElementById("pain4");
pain5 = document.getElementById("pain5");
panier = document.getElementById("panier");
audioPain = new Audio("../audios/Part1B_pain.mp3");
var painFirstClick = 1;

window.onload = hidePains();
pain5.addEventListener("click", lastPain);

function preLaunchAnim(){
    showPains();
    window.addEventListener("click", launchAnim);
}

function launchAnim(){
        setTimeout(movePain1, 500);
        setTimeout(movePain2, 1000);
        setTimeout(movePain3, 1800);
        setTimeout(movePain4, 3000);
}

function lastPain(){
    audioPain.play();
    console.log(audioPain);
    pain5.style.opacity = "0";
    panier.style.opacity = "0";
    setTimeout(goToDiary, 5000);
    document.body.style.cursor = "none";
}

function movePain1(){
    pain1.style.transform = "translate(-200vh, +200ch)";
}

function movePain2(){
    pain2.style.transform = "translate(-200vh, -200vh)";
}

function movePain3(){
    pain3.style.transform = "translate(+200vh, +200vh)";
}

function movePain4(){
    pain4.style.transform = "translate(+200vh, -200vh)";
}

function hidePains(){
    pain1.style.display = "none";
    pain2.style.display = "none";
    pain3.style.display = "none";
    pain4.style.display = "none";
    pain5.style.display = "none";
    panier.style.display = "none";
    etiquette.style.display = "none";
}

function showPains(){
    document.body.style.cursor = "default";
    pain1.style.display = "block";
    pain2.style.display = "block";
    pain3.style.display = "block";
    pain4.style.display = "block";
    pain5.style.display = "block";
    panier.style.display = "block";
}

function goToDiary(){
    window.location.href="../Part1T/Part1BT.html";
}