var ww = window.innerWidth,
  wh = window.innerHeight;
var torusRadius = 200;
var torusDiameter = 25;
var rings = 180;
var detail = 80;

var renderer = new THREE.WebGLRenderer({
  canvas: document.querySelector("canvas"),
  antialias : true
});
renderer.setSize(ww, wh);

var scene = new THREE.Scene();
scene.fog = new THREE.Fog(0x000000, torusRadius * 0.8, torusRadius * 1.1);

var camera = new THREE.PerspectiveCamera(60, ww / wh, 1, torusRadius * 1.1);
camera.position.set(Math.cos(0) * torusRadius, 10, Math.sin(0) * torusRadius);

var light = new THREE.PointLight(0xffffff, 2, 150);
light.position.set(Math.cos(-Math.PI * 0.3) * torusRadius, 0, Math.sin(-Math.PI * 0.3) * torusRadius);
scene.add(light);

TweenMax.to(light.position, 6, {
  x : Math.cos(Math.PI * 0.3) * torusRadius,
  z : Math.sin(Math.PI * 0.3) * torusRadius,
  ease: Power2.easeInOut,
  repeat:-2,
  yoyo :true
});

window.addEventListener("resize", function() {
  ww = window.innerWidth;
  wh = window.innerHeight;

  camera.aspect = ww / wh;
  camera.updateProjectionMatrix();

  renderer.setSize(ww, wh);
});
var mouse = new THREE.Vector2(0,0);

var torus = new THREE.Object3D();
TweenMax.to(torus.rotation, 90,{
  y:  Math.PI * 2,
  ease: Linear.easeNone,
  repeat: -1
});
scene.add(torus);

function createTorus() {
  var geometry = new THREE.BoxBufferGeometry(2, 2, 2);
  for (var i = 0; i < rings; i++) {
    var u = i / rings * Math.PI * 2;
    var ring = new THREE.Object3D();
    ring.position.x = torusRadius * Math.cos(u);
    ring.position.z = torusRadius * Math.sin(u);
    var colorIndex = Math.round(Math.abs(noise.simplex2(Math.cos(u) * 0.5, Math.sin(u) * 0.5)) * 180);
    var color = new THREE.Color("hsl(" + colorIndex + ",50%,50%)");
    /*var color = new THREE.Color("hsl(50,50%,50%)");*/
    var material = new THREE.MeshLambertMaterial({
      color: color
    });
    for (var j = 0; j < detail; j++) {
      var v = j / detail * Math.PI * 2;
      var x = torusDiameter * Math.cos(v) * Math.cos(u);
      var y = torusDiameter * Math.sin(v);
      var z = torusDiameter * Math.cos(v) * Math.sin(u);
      var size = (Math.random() * 5) + 0.1;
      var cube = new THREE.Mesh(geometry, material);
      cube.scale.set(size, size, size);
      cube.position.set(x, y, z);
      var rotation = (Math.random()-0.5)*Math.PI*4;
      cube.rotation.set(rotation, rotation, rotation);
      ring.add(cube);
    }
    torus.add(ring);
  }
}

function render() {
  requestAnimationFrame(render);
  camera.lookAt(light.position);
  renderer.render(scene, camera);
}

createTorus();
requestAnimationFrame(render);





  
//Apparition events

$.getScript("https://code.jquery.com/jquery-3.5.0.js");

function fade() {
    $( "#parent1:hidden" ).first().fadeIn("slow")};

function fadef9(){
    $("#f9:hidden").first().fadeIn(3000)};

function fadef10(){
    $("#f10:hidden").first().fadeIn(3000)};

function fadef11(){
    $("#f11:hidden").first().fadeIn(3000)};

function fadef15(){
    $("#f15:hidden").first().fadeIn(3000)};

function fadef12(){
    $("#f12:hidden").first().fadeIn(3000)};

function fadef13(){
    $("#f13:hidden").first().fadeIn(3000)};

function fadef14(){
    $("#f14:hidden").first().fadeIn(3000)};





//Déplacement mots en porte

n4=0;
n5=0;
n6=0;
n7=0;
n8=0;
n9=0;
n10=0;
n11=0;
n12=0;
n13=0;
n14=0;
n15=0;

somme=(n4+n5+n6+n7+n8+n9+n10+n11+n12+n13+n14+n15);

function mot(){
    
    var f4 = document.getElementById('f4');
    f4.addEventListener('mouseenter', function (event){
    event.target.style.transform = 'translateY(-360px)';
    event.target.style.transform += 'translateX(-300px)';
    event.target.style.transform += 'rotate(-90deg)';
    n4=1;
    somme=(n4+n5+n6+n7+n8+n9+n10+n11+n12+n13+n14+n15);
    if (somme==12){
        fade();
    };
    },{once : true});

    var f5 = document.getElementById('f5');
    f5.addEventListener('mouseenter', function(event){
    event.target.style.transform = 'translateY(360px)';
    event.target.style.transform += 'translateX(50px)';
    event.target.style.transform += 'rotate(90deg)';
    n5=1;
    somme=(n4+n5+n6+n7+n8+n9+n10+n11+n12+n13+n14+n15);
    if (somme==12){
        fade();
    };
    },{once : true});

    var f6 = document.getElementById('f6');
    f6.addEventListener('mouseenter', function(event){
    event.target.style.transform = 'translateY(330px)';
    event.target.style.transform += 'translateX(-460px)';
    event.target.style.transform += 'rotate(90deg)';
    n6=1;
    somme=(n4+n5+n6+n7+n8+n9+n10+n11+n12+n13+n14+n15);
    if (somme==12){
        fade();
    };
    },{once : true});

    var f7 = document.getElementById('f7');
    f7.addEventListener('mouseenter', function(event){
    event.target.style.transform = 'translateY(220px)';
    event.target.style.transform += 'translateX(-75px)';
    event.target.style.transform += 'rotate(90deg)';
    n7=1;
    somme=(n4+n5+n6+n7+n8+n9+n10+n11+n12+n13+n14+n15);
    if (somme==12){
        fade();
    };
    },{once : true});

    var f8 = document.getElementById('f8');
    f8.addEventListener('mouseenter', function(event){
    event.target.style.transform = 'translateY(-110px)';
    event.target.style.transform += 'translateX(-300px)';
    event.target.style.transform += 'rotate(-90deg)';
    n8=1;
    somme=(n4+n5+n6+n7+n8+n9+n10+n11+n12+n13+n14+n15);
    if (somme==12){
        fade();
    };
    },{once : true});

    var f9 = document.getElementById('f9');
    f9.addEventListener('mouseenter', function(event){
    event.target.style.transform = 'translateY(-225px)';
    event.target.style.transform += 'translateX(425px)';
    n9=1;
    somme=(n4+n5+n6+n7+n8+n9+n10+n11+n12+n13+n14+n15);
    if (somme==12){
        fade();
    };
    },{once : true});

    var f10 = document.getElementById('f10');
    f10.addEventListener('mouseenter', function(event){
    event.target.style.transform = 'translateY(-370px)';
    event.target.style.transform += 'translateX(-175px)';
    event.target.style.transform += 'rotate(-90deg)';
    n10=1;
    somme=(n4+n5+n6+n7+n8+n9+n10+n11+n12+n13+n14+n15);
    if (somme==12){
        fade();
    };
    },{once : true});

    var f11 = document.getElementById('f11');
    f11.addEventListener('mouseenter', function(event){
    event.target.style.transform = 'translateY(45px)';
    event.target.style.transform += 'translateX(240px)';
    n11=1;
    somme=(n4+n5+n6+n7+n8+n9+n10+n11+n12+n13+n14+n15);
    if (somme==12){
        fade();
    };
    },{once : true});

    var f12 = document.getElementById('f12');
    f12.addEventListener('mouseenter', function(event){
    event.target.style.transform = 'translateY(75px)';
    event.target.style.transform += 'translateX(-400px)';
    n12=1;
    somme=(n4+n5+n6+n7+n8+n9+n10+n11+n12+n13+n14+n15);
    if (somme==12){
        fade();
    };
    },{once : true});

    var f13 = document.getElementById('f13');
    f13.addEventListener('mouseenter', function(event){
    event.target.style.transform = 'translateY(-100px)';
    event.target.style.transform += 'translateX(320px)';
    n13=1;
    somme=(n4+n5+n6+n7+n8+n9+n10+n11+n12+n13+n14+n15);
    if (somme==12){
        fade();
    };
    },{once : true});

    var f14 = document.getElementById('f14');
    f14.addEventListener('mouseenter', function(event){
    event.target.style.transform = 'translateY(-215px)';
    event.target.style.transform += 'translateX(-45px)';
    event.target.style.transform += 'rotate(-90deg)';
    n14=1;
    somme=(n4+n5+n6+n7+n8+n9+n10+n11+n12+n13+n14+n15);
    if (somme==12){
        fade();
        };
    },{once : true});


    var f15 = document.getElementById('f15');
    f15.addEventListener('mouseenter', function(event){
    event.target.style.transform = 'translateY(410px)';
    event.target.style.transform += 'translateX(-265px)';
    event.target.style.transform += 'rotate(90deg)';
    n15=1;
    somme=(n4+n5+n6+n7+n8+n9+n10+n11+n12+n13+n14+n15);
    if (somme==12){
        fade();
        };
    },{once : true});
};



//choix

function Start(){
  setTimeout(fadef9, 2000);
  setTimeout(fadef10, 5000);
  setTimeout(fadef11, 8500);
  setTimeout(launchChoiceTunnel,11800);
};

function Suite(){
setTimeout(fadef15, 3000);
setTimeout(fadef12, 6000);
setTimeout(fadef13, 8000);
setTimeout(fadef14, 9500);
setTimeout(mot, 11000);
};


rightChoice1 = document.getElementById("rightChoice1");
leftChoice1 = document.getElementById("leftChoice1");
audioChoix = new Audio("../audios/tunnel_choix.mp3");
ChoiceButton = document.getElementById("ChoiceButton");



function launchChoiceTunnel(){
rightChoice1.style.opacity = "1";
leftChoice1.style.opacity = "1";
document.body.style.cursor = "auto";
rightChoice1.style.cursor = "pointer";
leftChoice1.style.cursor = "pointer";
}



$( "#leftChoice1" ).click(function() {
$("#leftChoice1" ).fadeOut({duration:2000,easing:"linear"});
audioChoix.play();
setTimeout(function(){$("#rightChoice1" ).fadeOut(2000,"linear");},2000);
setTimeout(Suite,4000);
setTimeout(function(){ChoiceButton.style.zIndex = "1"},4000);
});

$( "#rightChoice1" ).click(function() {
$("#leftChoice1" ).fadeOut(2000,"linear",myalert);
$("#rightChoice1" ).fadeOut(2000,"linear",myalert);
setTimeout(Suite,4000);
setTimeout(function(){ChoiceButton.style.zIndex = "1"},4000);
});



// audio

filterMusic = document.getElementById("filterMusic");
audioTrack = new Audio("../audios/tunnel3.mp3");

function launchStory(){
  filterMusic.style.display = "none";
  audioTrack.play();
  Start();
  console.log("launch");
}

